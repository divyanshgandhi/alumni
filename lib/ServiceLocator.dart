import 'package:get_it/get_it.dart';

import 'services/AuthService.dart';

GetIt locator = GetIt.I;

void setUpLocater() {
  locator.registerLazySingleton(() => AuthService());
}
