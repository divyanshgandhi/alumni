import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserModel {
  final String uid;
  final String name;
  final String reg;
  final String email;

  UserModel({
    this.uid,
    this.name,
    this.reg,
    this.email,
  });

  UserModel copyWith({
    String uid,
    String name,
    String reg,
    String email,
  }) {
    return UserModel(
      uid: uid ?? this.uid,
      name: name ?? this.name,
      reg: reg ?? this.reg,
      email: email ?? this.email,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'name': name,
      'reg': reg,
      'email': email,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    return UserModel(
      uid: map['uid'],
      name: map['name'],
      reg: map['reg'],
      email: map['email'],
    );
  }

  factory UserModel.fromDoc({@required DocumentSnapshot ds}) {
    final Map<String, dynamic> data = ds.data();
    return UserModel(
      email: data['email'],
      uid: data['uid'],
      name: data['name'],
      reg: data['reg'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UserModel(uid: $uid, name: $name, reg: $reg, email: $email)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is UserModel &&
        o.uid == uid &&
        o.name == name &&
        o.reg == reg &&
        o.email == email;
  }

  @override
  int get hashCode {
    return uid.hashCode ^ name.hashCode ^ reg.hashCode ^ email.hashCode;
  }
}
