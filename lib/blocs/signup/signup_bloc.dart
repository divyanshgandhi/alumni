import 'dart:async';

import 'package:alumni/ServiceLocator.dart';
import 'package:alumni/models/User.dart';
import 'package:alumni/services/AuthService.dart';
import 'package:alumni/widgets/Spinner.dart';
import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'signup_event.dart';
part 'signup_state.dart';
part 'signup_page.dart';

class SignupBloc extends Bloc<SignupEvent, SignupState> {
  SignupBloc() : super(SignupInitial());
  FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  Stream<SignupState> mapEventToState(
    SignupEvent event,
  ) async* {
    yield SignupLoading();
    // TODO: implement SignupInitialEvent
    if (event is SignupInitialEvent) {
      yield SignupInitial();
    }

    //TODO: implement InitiateSignup event
    if (event is InitiateSignup) {
      yield SignupValidation();
      UserModel userModel;
      User user;
      bool valid = false;
      bool db = false;

      valid = await locator<AuthService>()
          .validateUser(name: event.name, reg: event.reg);
      if (valid) {
        try {
          await locator<AuthService>().createUserWithEmailAndPassword(
              email: event.email, password: event.password);
          user = _auth.currentUser;
          userModel = UserModel(
            uid: user.uid,
            name: event.name,
            reg: event.reg,
            email: event.email,
          );
          db = await locator<AuthService>().writeToDB(userModel: userModel);
          if (user != null && db) {
            yield SignupSuccess(user: userModel);
          } else {
            yield SignupFailed(error: "User is null or db error");
          }
        } catch (e) {
          print(e.toString());
          yield SignupFailed(error: e);
        }
      } else {
        yield ValidationFailed();
      }
    }
  }
}
