part of 'signup_bloc.dart';

@immutable
abstract class SignupState {}

class SignupInitial extends SignupState {}

class SignupValidation extends SignupState {}

class ValidationFailed extends SignupState {}

class SignupSuccess extends SignupState {
  final UserModel user;

  SignupSuccess({@required this.user});
}

class SignupFailed extends SignupState {
  final dynamic error;

  SignupFailed({@required this.error});
}

class TryAgain extends SignupState {}

class SignupLoading extends SignupState {}
