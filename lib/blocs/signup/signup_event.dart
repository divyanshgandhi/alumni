part of 'signup_bloc.dart';

@immutable
abstract class SignupEvent {}

class SignupInitialEvent extends SignupEvent {}

class InitiateSignup extends SignupEvent {
  final String name;
  final String reg;
  final String email;
  final String password;

  InitiateSignup({
    @required this.name,
    @required this.reg,
    @required this.email,
    @required this.password,
  });
}

class TryAgainEvent extends SignupEvent {
  final String name;
  final String reg;
  final String email;

  TryAgainEvent({
    @required this.name,
    @required this.reg,
    @required this.email,
  });
}
