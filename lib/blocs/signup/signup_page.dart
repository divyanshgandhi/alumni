part of 'signup_bloc.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _regController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _passwordHidden;

  @override
  void initState() {
    super.initState();
    _passwordHidden = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Signup Demo"),
      ),
      body: BlocConsumer<SignupBloc, SignupState>(
          builder: (context, state) {
            if (state is SignupLoading) {
              return Spinner();
            }

            if (state is SignupInitial) {
              return ListView(children: [
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 72.0,
                      ),
                      Text(
                        'Name',
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 18.0, horizontal: 72.0),
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter name';
                            }
                            return null;
                          },
                          controller: _nameController,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.person,
                                color: Colors.white,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(90.0),
                                ),
                                borderSide: BorderSide.none,
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.white,
                                  fontFamily: "WorkSansLight"),
                              filled: true,
                              fillColor: Colors.grey,
                              hintText: 'Name'),
                        ),
                      ),
                      Text(
                        'Registration number',
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 18.0, horizontal: 72.0),
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter reg number';
                            }
                            return null;
                          },
                          controller: _regController,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.confirmation_number,
                                color: Colors.white,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(90.0),
                                ),
                                borderSide: BorderSide.none,
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.white,
                                  fontFamily: "WorkSansLight"),
                              filled: true,
                              fillColor: Colors.grey,
                              hintText: 'Registration no.'),
                        ),
                      ),
                      Text(
                        'Email',
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 18.0, horizontal: 72.0),
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter email';
                            }
                            return null;
                          },
                          controller: _emailController,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.mail,
                                color: Colors.white,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(90.0),
                                ),
                                borderSide: BorderSide.none,
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.white,
                                  fontFamily: "WorkSansLight"),
                              filled: true,
                              fillColor: Colors.grey,
                              hintText: 'Email'),
                        ),
                      ),
                      Text(
                        'Password',
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 18.0, horizontal: 72.0),
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter password';
                            }
                            return null;
                          },
                          controller: _passwordController,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              errorStyle: TextStyle(
                                color: Colors.white,
                              ),
                              prefixIcon: Icon(
                                Icons.lock,
                                color: Colors.white,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(90.0),
                                ),
                                borderSide: BorderSide.none,
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.white,
                                  fontFamily: "WorkSansLight"),
                              filled: true,
                              fillColor: Colors.grey,
                              hintText: 'Password'),
                        ),
                      ),
                      SizedBox(height: 24.0),
                      GestureDetector(
                        onTap: () async {
                          if (_formKey.currentState.validate()) {
                            context.read<SignupBloc>().add(
                                  InitiateSignup(
                                    name: _nameController.text,
                                    reg: _regController.text,
                                    email: _emailController.text,
                                    password: _passwordController.text,
                                  ),
                                );
                          }
                        },
                        child: Container(
                          height: 48,
                          width: 72,
                          color: Colors.blue,
                          child: Center(
                            child: Text("Sign Up",
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ]);
            }

            if (state is SignupValidation) {
              return Center(child: Text("Validating user"));
            }

            if (state is SignupSuccess) {
              return Center(child: Text(state.user.toString()));
            }

            if (state is SignupFailed) {
              return Center(child: Text(state.error.toString()));
            }

            if (state is ValidationFailed) {
              return Center(
                  child: Text("Validation failed, check your details again."));
            }

            return Container();
          },
          listener: (context, state) {}),
    );
  }
}
