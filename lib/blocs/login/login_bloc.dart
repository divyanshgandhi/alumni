import 'dart:async';

import 'package:alumni/models/User.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';

part 'login_event.dart';
part 'login_page.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    //TODO: Implement InitialEvent
    if (event is InitialEvent) {
      yield LoginInitial();
    }

    //TODO: Implement InitiateLogin event
    if (event is InitiateLogin) {
      try {
        UserModel user;
        yield LoginSuccesful(user: user);
      } catch (e) {
        yield LoginFailed(error: e);
      }
    }

    //TODO: Implement TryAgainEvent
    if (event is TryAgainEvent) {
      yield TryAgainState(email: event.email);
    }
  }
}
