part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

//First load of Login, default page
class InitialEvent extends LoginEvent {}

//Click on Login button, initiate Login
class InitiateLogin extends LoginEvent {
  final String email;
  final String password;

  InitiateLogin({
    @required this.email,
    @required this.password,
  });

  List<Object> get props => [
        email,
        password,
      ];
}

//Login failed, try again event
class TryAgainEvent extends LoginEvent {
  final String email;

  TryAgainEvent({
    @required this.email,
  });

  List<Object> get props => [
        email,
      ];
}
