part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginSuccesful extends LoginState {
  final UserModel user;

  LoginSuccesful({@required this.user});
}

class LoginFailed extends LoginState {
  final dynamic error;

  LoginFailed({@required this.error});
}

class TryAgainState extends LoginState {
  final String email;

  TryAgainState({@required this.email});
}
