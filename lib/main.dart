import 'package:alumni/services/AuthService.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'ServiceLocator.dart';
import 'blocs/signup/signup_bloc.dart';
import 'models/User.dart';

void main() async {
  setUpLocater();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Alumni Demo Login'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String email;
  String password;
  UserModel user;
  bool signedIn = false;
  bool details = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 72.0,
            ),
            GestureDetector(
              onTap: () {
                Route route = MaterialPageRoute(
                  builder: (BuildContext context) => BlocProvider(
                    create: (BuildContext context) => SignupBloc(),
                    child: SignupPage(),
                  ),
                );
                Navigator.push(context, route);
              },
              child: Text(
                "Go to Sign up",
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: 24.0,
                ),
              ),
            ),
            SizedBox(height: 48.0),
            Text(
              'Email',
            ),
            Padding(
              padding: EdgeInsets.all(18.0),
              child: TextFormField(
                controller: _email,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                    errorStyle: TextStyle(
                      color: Colors.white,
                    ),
                    prefixIcon: Icon(
                      Icons.person,
                      color: Colors.white,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(90.0),
                      ),
                      borderSide: BorderSide.none,
                    ),
                    hintStyle: TextStyle(
                        color: Colors.white, fontFamily: "WorkSansLight"),
                    filled: true,
                    fillColor: Colors.grey,
                    hintText: 'Email'),
              ),
            ),
            Text(
              'Password',
            ),
            Padding(
              padding: EdgeInsets.all(18.0),
              child: TextFormField(
                controller: _password,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                    errorStyle: TextStyle(
                      color: Colors.white,
                    ),
                    prefixIcon: Icon(
                      Icons.lock,
                      color: Colors.white,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(90.0),
                      ),
                      borderSide: BorderSide.none,
                    ),
                    hintStyle: TextStyle(
                        color: Colors.white, fontFamily: "WorkSansLight"),
                    filled: true,
                    fillColor: Colors.grey,
                    hintText: 'Password'),
              ),
            ),
            SizedBox(height: 24.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () async {
                    UserModel temp;
                    if (_email != null && _password != null) {
                      email = _email.text;
                      password = _password.text;
                    } else {
                      print("Null error on controller");
                    }
                    if (email != null && password != null) {
                      await AuthService().signInWithEmailAndPassword(
                          email: email, password: password);
                      temp = await AuthService().getCurrentUser();
                      signedIn = await locator<AuthService>().validateUser(
                          name: "Divyansh Gandhi", reg: "RA1811003030570");
                      print(signedIn);
                      setState(() {
                        user = temp;
                        signedIn = true;
                      });
                    } else {
                      print("Null error on text fields");
                    }
                  },
                  child: Container(
                    height: 48,
                    width: 72,
                    color: Colors.blue,
                    child: Center(
                      child: Text("Sign in",
                          style: TextStyle(color: Colors.white)),
                    ),
                  ),
                ),
                SizedBox(width: 24.0),
                GestureDetector(
                  onTap: () {
                    if (user != null) {
                      setState(() {
                        details = true;
                      });
                    }
                  },
                  child: Container(
                    height: 48,
                    width: 72,
                    color: Colors.blue,
                    child: Center(
                      child: Text("Get Details",
                          style: TextStyle(color: Colors.white)),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 24.0),
            Visibility(
              visible: signedIn,
              child: Text(
                "Sign in successful",
                style: TextStyle(
                  color: Colors.green,
                ),
              ),
            ),
            SizedBox(height: 24.0),
            Visibility(
              visible: details,
              child: Text(
                user.toString(),
                style: TextStyle(
                  color: Colors.blue,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
